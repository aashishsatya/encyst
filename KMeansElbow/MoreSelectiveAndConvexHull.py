# now the value has to be the highest in both the row and column

# load the data
import pickle
from collections import Counter
from scipy.spatial import ConvexHull
import time
import datetime

data_file = open('data.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = [row[:-1] for row in data_with_ans]

rows_with_col_max = {}  # dict of the form <column index>: [rows for which the the maximum value in the row occurs in that column]

for i in range(len(data)):
    max_val = max(data[i])
    max_indices = [j for j, x in enumerate(data[i]) if x == max_val]
    for max_index in max_indices:
        if max_index not in rows_with_col_max:
            rows_with_col_max[max_index] = []
        rows_with_col_max[max_index].append(i)

selective_rows_with_col_max = {} # dict of the form <column index>: [rows which correspond to the maximum value in the column]

for key in rows_with_col_max:
    values = [data[row][key] for row in rows_with_col_max[key]]
    max_value = max(values)
    max_indices = [j for j, x in enumerate(values) if x == max_val]
    if len(max_indices) == 0:
        continue
    selective_rows_with_col_max[key] = [rows_with_col_max[key][index] for index in max_indices]

rows_to_select = set([])

for key in selective_rows_with_col_max:
    for row in selective_rows_with_col_max[key]:
        rows_to_select.add(row)
print('Number of rows =', len(rows_to_select))

rows_for_convex_hull = [data[row] for row in rows_to_select]
print('Computing convex hull...')
start_day_time = datetime.datetime.now()
print(start_day_time.strftime("Start time: %Y-%m-%d %H:%M:%S"))
start = time.time()
hull = ConvexHull(rows_for_convex_hull)
stop = time.time()
stop_day_time = datetime.datetime.now()
print('...done.')
print(stop_day_time.strftime("Stop time: %Y-%m-%d %H:%M:%S"))
print('Time taken to compute convex hull =', start - stop)