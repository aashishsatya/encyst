# code for k-means clustering

import numpy as np
from scipy.special import factorial
import matplotlib.pyplot as plt
import pickle
# import sys

def k_poisson_probs(points, k, epsilon = 0.001, max_iter = 30, max_init_count = 5):

    points = np.array(points)
    # TODO: find how the answer changes w.r.t. this constant
    const = 5
    points = points + const # so that you don't get any zeros

    N = len(points)
    dim = len(points[0])

    best_log_sum = -float('inf')

    for init_count in range(max_init_count):

        print('Initialization', init_count + 1, '...')

        # randomly draw k points
        # pt_indices = np.random.choice(range(len(points)), k, replace = False)
        # cluster_sum = np.array([points[i] for i in pt_indices])  # this will store the SUM of all points in a cluster for each cluster
        # cluster_len = np.array([1] * k)

        # shuffle the points randomly
        np.random.shuffle(points)

        points_per_cluster = N // k
        cluster_sum = np.zeros((k, dim))
        cluster_len = np.array([0] * k).reshape((k, 1))
        # cluster_len = np.array([0] * k)
        pt_to_cluster = {}

        for i in range(k):
            points_in_cluster = points[i * points_per_cluster: (i + 1) * points_per_cluster]
            cluster_sum[i] = np.sum(points_in_cluster, axis = 0, keepdims = True)
            cluster_len[i] = len(points_in_cluster)
            for j in range(i * points_per_cluster, (i + 1) * points_per_cluster):
                pt_to_cluster[j] = i

        for i in range(N):

            point = points[i]
            best_cluster = -1
            best_cluster_prob = -float('inf')
            for j in range(k):
                # probability that the point is drawn from cluster j
                centroid = cluster_sum[j] / cluster_len[j]
                attrib_probs = np.multiply(np.power(centroid, point), np.exp(-1 * centroid)) / factorial(point)
                attrib_log_probs = np.sum(np.log(attrib_probs))
                if attrib_log_probs > best_cluster_prob:
                    best_cluster_prob = attrib_log_probs
                    best_cluster = j
            pt_to_cluster[i] = best_cluster
            cluster_sum[best_cluster] += points[i]
            cluster_len[best_cluster] += 1  # one more point just got added to the cluster
        
        # initialization done
        # now do at most max_iter iterations of k-means

        for kmeans_itercount in range(max_iter):

            old_clusters = cluster_sum / cluster_len[:, None]

            for i in range(N):

                # find the best new cluster for point i
                point = points[i]

                # first remove point i from its old cluster
                cluster_sum[pt_to_cluster[i]] -= point
                cluster_len[pt_to_cluster[i]] -= 1

                best_cluster = -1
                best_cluster_prob = -float('inf')
                for j in range(k):
                    # probability that the point is drawn from cluster j
                    if cluster_len[j] == 0:
                        # that point got lucky
                        # it can just go to that cluster and sit
                        best_cluster = j
                        break
                    centroid = cluster_sum[j] / cluster_len[j]  #TODO: this is inefficient
                    attrib_probs = np.multiply(np.power(centroid, point), np.exp(-1 * centroid)) / factorial(point)
                    attrib_log_probs = np.sum(np.log(attrib_probs))
                    if attrib_log_probs > best_cluster_prob:
                        best_cluster_prob = attrib_log_probs
                        best_cluster = j
                
                # bookkeeping of pt_to_cluster
                # move the point i from the old cluster it was in to the new cluster
                
                pt_to_cluster[i] = best_cluster
                cluster_sum[best_cluster] += points[i]
                cluster_len[best_cluster] += 1  # one more point just got added to the cluster

            new_clusters = cluster_sum / cluster_len[:, None]
            eps = np.max(abs(new_clusters - old_clusters))
        
            if eps < epsilon:
                break

        # return the product of probabilities (sum of log probabilities) for each cluster
        # this is what will be used in lieu of SSE

        centroids = cluster_sum / cluster_len[:, None]

        log_prob_sum = 0
        for i in range(N):
            centroid = centroids[pt_to_cluster[i]]
            attrib_probs = (np.power(centroid, point) * np.exp(-1 * centroid)) / factorial(point)
            attrib_log_probs = np.sum(np.log(attrib_probs))
            log_prob_sum += attrib_log_probs

        if log_prob_sum > best_log_sum:
            best_log_sum = log_prob_sum

        print('Cluster len:')
        print(cluster_len.reshape((1, k)))

    return best_log_sum    

def do_kmeans(data, hi_val):

    log_prob_sum = {}
    for k in range(20, 201, 20):
    # for k in [10,20]:
        print('Computing for k =', k, '...')
        prob = k_poisson_probs(data, k, 0.001)
        print('Best poisson prob =', prob)
        log_prob_sum[k] = prob
    fig = plt.figure()
    plt_title = 'Log prob sum vs k for avg high expression seq count = ' + str(hi_val)
    plt.plot(list(log_prob_sum.keys()), list(log_prob_sum.values()))
    plt.xlabel("Number of clusters")
    plt.ylabel("Sum of log probabilities")
    plt.title(plt_title)
    # plt.show()
    plt.savefig('log_prob_graph' + str(hi_val) + '.svg', format = 'svg', dpi = 1200)
    plt.close(fig)

def plot_sse(hi_vals):

    for hi_val in hi_vals:

        print('Plotting graph for hi_val =', hi_val)

        # load the data

        data_file = open('data' + str(hi_val) + '.csv', 'rb')
        data_with_ans = pickle.load(data_file)
        data_file.close()

        ans_file = open('ans' + str(hi_val) + '.csv', 'rb')
        ans = pickle.load(ans_file)
        ans_file.close()

        data = [row[:-1] for row in data_with_ans]
        do_kmeans(data, hi_val)

# hi_vals = [10, 5, 1, 0.5, 0.1]
hi_vals = [10]
plot_sse(hi_vals)    

