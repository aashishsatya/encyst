# code for k-means clustering

from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import pickle

def do_kmeans(data, hi_val):

    sse = {}
    for k in range(10, 205, 5):
        print('Computing for k =', k, '...')
        kmeans = KMeans(n_clusters = k, max_iter = 30, verbose = 0).fit(data)
        sse[k] = kmeans.inertia_ # Inertia: Sum of distances of samples to their closest cluster center
    fig = plt.figure()
    plt_title = 'SSE vs k for avg high expression seq count = ' + str(hi_val)
    plt.xlabel("Number of clusters")
    plt.ylabel("SSE")
    plt.title(plt_title)
    # plt.show()
    plt.plot(list(sse.keys()), list(sse.values()))
    plt.savefig('graph' + str(hi_val) + '.svg', bbox_inches = 'tight', format = 'svg', dpi = 1200)
    plt.close(fig)

def plot_sse(hi_vals):

    for hi_val in hi_vals:

        print('Plotting graph for hi_val =', hi_val)

        # load the data

        data_file = open('../Data/data' + str(hi_val) + '.csv', 'rb')
        data_with_ans = pickle.load(data_file)
        data_file.close()

        ans_file = open('../Data/ans' + str(hi_val) + '.csv', 'rb')
        ans = pickle.load(ans_file)
        ans_file.close()

        data = [row[:-1] for row in data_with_ans]
        do_kmeans(data, hi_val)

# hi_vals = [10]
hi_vals = [10, 5, 1, 0.5, 0.1]
plot_sse(hi_vals)

