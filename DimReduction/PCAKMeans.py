# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

def do_kmeans(data, hi_val):

    sse = {}
    for k in range(10, 205, 5):
        print('Computing for k =', k, '...')
        kmeans = KMeans(n_clusters = k, max_iter = 30, verbose = 0).fit(data)
        sse[k] = kmeans.inertia_ # Inertia: Sum of distances of samples to their closest cluster center
    fig = plt.figure()
    plt_title = 'SSE vs k for avg high expression seq count = ' + str(hi_val)
    plt.plot(list(sse.keys()), list(sse.values()))
    plt.xlabel("Number of cluster")
    plt.ylabel("SSE")
    plt.title(plt_title)
    # plt.show()
    plt.savefig('PCAKMeans' + str(hi_val) + '.png', bbox_inches = 'tight')
    plt.close(fig)

gene_on_means = [0.1, 0.5, 1, 5, 10]

for gene_on_mean in gene_on_means:

    data_file_name = '../Data/data' + str(gene_on_mean) + '.csv'
    ans_file_name = '../Data/ans' + str(gene_on_mean) + '.csv'

    print('Results for mean value of', gene_on_mean, ':')

    data_file = open(data_file_name, 'rb')
    data_with_ans = pickle.load(data_file)
    data_file.close()

    ans_file = open(ans_file_name, 'rb')
    ans = pickle.load(ans_file)
    ans_file.close()

    data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
    N = len(data)

    data_mean = np.mean(data, axis = 0)
    print('Calculating pairwise distances...')
    data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the data
    data -= data_mean
    data /= np.sqrt(dist_variance)

    print('Performing PCA...')
    pca = PCA(n_components = 2)
    pca_data = pca.fit_transform(data)

    do_kmeans(pca_data, gene_on_mean)