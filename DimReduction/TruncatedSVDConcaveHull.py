# code for concave hull courtesy of https://github.com/sebastianbeyer/concavehull

# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial
from sklearn.decomposition import TruncatedSVD
import ConcaveHull as ccv_hull

# gene_on_means = [0.1, 0.5, 1, 5, 10]
gene_on_means = [0.1]

for gene_on_mean in gene_on_means:

    data_file_name = '../Data/data' + str(gene_on_mean) + '.csv'
    ans_file_name = '../Data/ans' + str(gene_on_mean) + '.csv'

    print('Results for mean value of', gene_on_mean, ':')

    data_file = open(data_file_name, 'rb')
    data_with_ans = pickle.load(data_file)
    data_file.close()

    ans_file = open(ans_file_name, 'rb')
    ans = pickle.load(ans_file)
    ans_file.close()

    data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
    N = len(data)

    data_mean = np.mean(data, axis = 0)
    print('Calculating pairwise distances...')
    data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the data
    data -= data_mean
    data /= np.sqrt(dist_variance)

    # do tsvd first as suggested by the blog post
    print('Performing truncated SVD...')
    tsvd = TruncatedSVD(n_components = 2)
    tsvd_data = tsvd.fit_transform(data)

    print('Computing concave hull...')
    hull = ccv_hull.concaveHull(tsvd_data, 5)
    ccv_hull.plotPath(tsvd_data, hull)

    # identify the indices of the points
    
    hull = hull[:-1]    # first point is repeated twice

    hull_indices = []
    for hull_pt in hull:
        for i in range(len(tsvd_data)):
            if np.all(hull_pt == tsvd_data[i]):
                hull_indices.append(i)
                break
    cell_types_detected = set([])
    for hull_index in hull_indices:
        cell_types_detected.add(data_with_ans[hull_index][-1])

    print('Number of points in the hull =', len(hull))
    print('Number of cell types caught =', len(cell_types_detected))