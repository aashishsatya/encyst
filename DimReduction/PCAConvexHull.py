# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial
from sklearn.decomposition import PCA

# data_file = open('../Data/data10.csv', 'rb')
# data_with_ans = pickle.load(data_file)
# data_file.close()

# ans_file = open('../Data/ans10.csv', 'rb')
# ans = pickle.load(ans_file)
# ans_file.close()

# data = np.array([row[:-1] for row in data_with_ans])
# N = len(data)
# print('Number of observations =', N)

# print('Performing PCA...')
# pca = PCA(n_components = 2)
# pca_data = pca.fit_transform(data)

# print('Computing convex hull...')
# hull = scipy.spatial.ConvexHull(pca_data)

# print('Number of points in the hull =', len(hull.vertices))

gene_on_means = [0.1, 0.5, 1, 5, 10]

for gene_on_mean in gene_on_means:

    data_file_name = '../Data/data' + str(gene_on_mean) + '.csv'
    ans_file_name = '../Data/ans' + str(gene_on_mean) + '.csv'

    print('Results for mean value of', gene_on_mean, ':')

    data_file = open(data_file_name, 'rb')
    data_with_ans = pickle.load(data_file)
    data_file.close()

    ans_file = open(ans_file_name, 'rb')
    ans = pickle.load(ans_file)
    ans_file.close()

    data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
    N = len(data)

    data_mean = np.mean(data, axis = 0)
    print('Calculating pairwise distances...')
    data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the data
    data -= data_mean
    data /= np.sqrt(dist_variance)

    print('Performing PCA...')
    pca = PCA(n_components = 4)
    pca_data = pca.fit_transform(data)

    print('Computing convex hull...')
    hull = scipy.spatial.ConvexHull(pca_data)

    print('Number of points in the hull =', len(hull.vertices))


