# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# gene_on_means = [0.1, 0.5, 1, 5, 10]
gene_on_means = [5]

for gene_on_mean in gene_on_means:

    data_file_name = '../Data/data' + str(gene_on_mean) + '.csv'
    ans_file_name = '../Data/ans' + str(gene_on_mean) + '.csv'

    print('Results for mean value of', gene_on_mean, ':')

    data_file = open(data_file_name, 'rb')
    data_with_ans = pickle.load(data_file)
    data_file.close()

    ans_file = open(ans_file_name, 'rb')
    ans = pickle.load(ans_file)
    ans_file.close()

    data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
    N = len(data)

    data_mean = np.mean(data, axis = 0)
    print('Calculating pairwise distances...')
    data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the data
    data -= data_mean
    data /= np.sqrt(dist_variance)

    print('Performing t-SNE...')
    tsne = TSNE(n_components = 2)
    tsne_data = tsne.fit_transform(data)

    # normalize the tsne_data
    # get the normalization parameters
    tsne_data_mean = np.mean(tsne_data, axis = 0)
    print('Calculating pairwise distances...')
    tsne_data_dist = scipy.spatial.distance.pdist(tsne_data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(tsne_data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the tsne_data
    tsne_data -= tsne_data_mean
    tsne_data /= np.sqrt(dist_variance)

    print('Calculating pairwise similarity after t-SNE...')
    sim_matrix = sklearn.metrics.pairwise.rbf_kernel(tsne_data, gamma = 1.0/2)
    # sim_matrix = sklearn.metrics.pairwise.cosine_similarity(tsne_data)
    print('...done.')

    degree_vector = 1/np.sum(sim_matrix, axis = 1, keepdims = True)
    degree_matrix = np.eye(N) * degree_vector	# should broadcast
    laplacian_rw = np.eye(N) - np.matmul(degree_matrix, sim_matrix)
    eigenvals = np.linalg.eigvalsh(laplacian_rw)
    eigenvals = eigenvals.tolist()
    eigenvals.sort()

    max_gap = -float('inf')
    eigenval_thresholds = [0.01, 0.05, 0.001, 0.005, 0.00001, 0.00005]   # should be slightly bigger than 0
    eigenval_thresholds.sort()

    for eigenval_threshold in eigenval_thresholds:
        best_k = len(eigenvals)
        for i in range(len(eigenvals)):
            if eigenvals[i] > eigenval_threshold:
                best_k = i
                break
        print('k for eigenvalue threshold of', eigenval_threshold, ':', best_k)