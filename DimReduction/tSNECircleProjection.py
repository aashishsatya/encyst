"""
Basic idea:
1. Do t-SNE
2. Project the points you get using t-SNE onto an appropriate circle
3. Number of points on the convex hull of the circle is your required k
"""

# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA

# from shapely.geometry import LineString
# from shapely.geometry import Point
# using Shapely to find the intersection - https://stackoverflow.com/questions/30844482/what-is-most-efficient-way-to-find-the-intersection-of-a-line-and-a-circle-in-py

def intersection(point, radius):

    """
    Find the intersection of a line drawn from origin passing through the given point and the circle
    x^2 + y^2 = radius^2
    """

    slope = point[1] / point[0]
    pt_x = np.sqrt(radius ** 2 / (1 + slope ** 2))
    pt_y = slope * pt_x
    return [pt_x, pt_y]

gene_on_means = [10, 5, 1, 0.5, 0.1]

for gene_on_mean in gene_on_means:

    data_file_name = '../Data/data' + str(gene_on_mean) + '.csv'
    ans_file_name = '../Data/ans' + str(gene_on_mean) + '.csv'

    print('Results for mean value of', gene_on_mean, ':')

    data_file = open(data_file_name, 'rb')
    data_with_ans = pickle.load(data_file)
    data_file.close()

    ans_file = open(ans_file_name, 'rb')
    ans = pickle.load(ans_file)
    ans_file.close()

    data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
    N = len(data)

    data_mean = np.mean(data, axis = 0)
    print('Calculating pairwise distances...')
    data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
    print('...done.')
    dist_variance = np.var(data_dist)
    print('Variance of the distances =', dist_variance)
    # we have the parameters, now actually normalize the data
    data -= data_mean
    data /= np.sqrt(dist_variance)

    # do PCA first as suggested by the blog post
    print('Performing PCA...')
    pca = PCA(n_components = 50)
    pca_data = pca.fit_transform(data)

    # do t-SNE
    print('Performing t-SNE...')
    tsne = TSNE(n_components = 2)
    tsne_data = tsne.fit_transform(pca_data)
    tsne_mean = np.mean(tsne_data, axis = 0)
    tsne_data -= tsne_mean  # now data is centred about the origin
    origin = np.array([0, 0])

    # now project onto circle

    # find the radius (max distance of a point from the mean)

    distances = [np.linalg.norm(tsne_pt - origin) for tsne_pt in tsne_data]
    circle_radius = max(distances) + 1 # + 1 so that all points are inside the circle

    projection_on_circle = []
    for tsne_pt in tsne_data:
        pt_projection = intersection(tsne_pt, circle_radius)
        projection_on_circle.append(pt_projection)

    print('Computing convex hull...')
    hull = scipy.spatial.ConvexHull(projection_on_circle)

    print('Number of points in the hull =', len(hull.vertices))


