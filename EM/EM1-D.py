import numpy
import matplotlib.pyplot as plt

mean1 = 0
mean2 = 0.5
arr1 = numpy.random.poisson(mean1, 5000)
arr2 = numpy.random.poisson(mean2, 5000)

arr = numpy.array(list(arr1) + list(arr2))

plt.hist(arr, bins = 'auto')  # arguments are passed to np.histogram
plt.title("RNA seq count vs Frequency")
plt.xlabel('RNA seq count')
plt.ylabel('Frequency')
plt.show()
