# code for k-means clustering

import numpy as np
from scipy.special import factorial
import matplotlib.pyplot as plt
import pickle
# import sys

def em(points, k, epsilon = 0.001, max_iter = 30, max_init_count = 5):

    points = np.array(points)
    

    return log_lik

def do_em(data, hi_val):

    log_prob_sum = {}
    for k in range(20, 201, 20):
    # for k in [10,20]:
        print('Computing for k =', k, '...')
        prob = em(data, k, 0.001)
        print('Best log likelihood =', prob)
        log_prob_sum[k] = prob
    fig = plt.figure()
    plt_title = 'Log prob sum vs k for avg high expression seq count = ' + str(hi_val)
    plt.plot(list(log_prob_sum.keys()), list(log_prob_sum.values()))
    plt.xlabel("Number of cluster")
    plt.ylabel("Sum of log probabilities")
    plt.title(plt_title)
    # plt.show()
    plt.savefig('log_prob_graph' + str(hi_val) + '.png')
    plt.close(fig)

def plot_sse(hi_vals):

    for hi_val in hi_vals:

        print('Plotting graph for hi_val =', hi_val)

        # load the data

        data_file = open('data' + str(hi_val) + '.csv', 'rb')
        data_with_ans = pickle.load(data_file)
        data_file.close()

        ans_file = open('ans' + str(hi_val) + '.csv', 'rb')
        ans = pickle.load(ans_file)
        ans_file.close()

        data = [row[:-1] for row in data_with_ans]
        do_kmeans(data, hi_val)

# hi_vals = [10, 5, 1, 0.5, 0.1]
hi_vals = [10, 5, 1, 0.5, 0.1]
plot_sse(hi_vals)    