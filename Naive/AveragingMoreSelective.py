# now the value has to be the highest in both the row and column

# load the data
import pickle
from collections import Counter

data_file = open('data.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = [row[:-1] for row in data_with_ans]

rows_with_col_max = {}  # dict of the form <column index>: [rows for which the the maximum value in the row occurs in that column]

for i in range(len(data)):
    max_val = max(data[i])
    max_indices = [j for j, x in enumerate(data[i]) if x == max_val]
    for max_index in max_indices:
        if max_index not in rows_with_col_max:
            rows_with_col_max[max_index] = []
        rows_with_col_max[max_index].append(i)

selective_rows_with_col_max = {} # dict of the form <column index>: [rows which correspond to the maximum value in the column]

for key in rows_with_col_max:
    values = [data[row][key] for row in rows_with_col_max[key]]
    max_value = max(values)
    max_indices = [j for j, x in enumerate(values) if x == max_val]
    if len(max_indices) == 0:
        continue
    selective_rows_with_col_max[key] = [rows_with_col_max[key][index] for index in max_indices]

for key in selective_rows_with_col_max:
    answers = [data_with_ans[index][-1] for index in selective_rows_with_col_max[key]]
    print(key, ':', Counter(answers))
print('Number of columns in dict =', len(selective_rows_with_col_max.keys()))