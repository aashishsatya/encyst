# now the value has to be the highest in both the row and column

# load the data
import pickle
from collections import Counter

data_file = open('data.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = [row[:-1] for row in data_with_ans]

for i in range(len(data_with_ans[0]) - 1):
    data_with_ans.sort(key = lambda x: x[i], reverse = True)
    print('Column', i, ':')
    print([data_with_ans[j][-1] for j in range(len(data))])