# load the data
import pickle
from collections import Counter

data_file = open('data.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = [row[:-1] for row in data_with_ans]

col_to_row_dict = {}

for i in range(len(data)):
    max_val = max(data[i])
    max_indices = [j for j, x in enumerate(data[i]) if x == max_val]
    for max_index in max_indices:
        if max_index not in col_to_row_dict:
            col_to_row_dict[max_index] = []
        col_to_row_dict[max_index].append(i)

# row_count = 0
# for key in col_to_row_dict:
#     print(key, ':', col_to_row_dict[key])
    # row_count += len(col_to_row_dict[key])
# print(row_count)
# print(len(data))
detected_cells = set([])
for key in col_to_row_dict:
    # answers.sort()
    answers = [data_with_ans[index][-1] for index in col_to_row_dict[key]]
    ctr_answers = Counter(answers)
    print(key, ':', ctr_answers)
    # for key in ctr_answers.keys():
    #     detected_cells.add(key)
    # print(key, ':', col_to_row_dict[key])
    # print(key, ':', answers)
print('Number of columns in dict =', len(col_to_row_dict.keys()))
# print('Detected cell types =', len(detected_cells))