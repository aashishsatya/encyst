# load the data
import pickle
from collections import Counter
import numpy as np

data_file = open('data0.5.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans0.5.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = np.array([row[:-1] for row in data_with_ans])
print('Number of observations =', len(data))
print('Size of original data =', data.nbytes)
# print(type(data[0][0]))

col_to_row_dict = {}

for i in range(len(data)):
    max_val = max(data[i])
    # max_indices = [j for j, x in enumerate(data[i]) if x == max_val]
    max_indices = [j for j, x in enumerate(data[i]) if x == max_val or x == max_val - 1]
    for max_index in max_indices:
        if max_index not in col_to_row_dict:
            col_to_row_dict[max_index] = []
        col_to_row_dict[max_index].append(i)

total_count = 0
pairs = set([])
# print('Number of columns =', len(col_to_row_dict))
for max_index in col_to_row_dict:
    max_indices = col_to_row_dict[max_index]
    max_indices.sort()
    for i in range(len(max_indices) - 1):
        for j in range(i + 1, len(max_indices)):
            if (i, j) not in pairs:
                pairs.add((i, j))
print('Number of points =', len(pairs))


new_data = []
for x, y in list(pairs):
    new_data.append((data[x] + data[y])/2.0)
new_data = np.array(new_data)
print('Size of new data =', new_data.nbytes)
# print(type(new_data[0][0]))
print('Number of points in new data:', len(new_data))
print('Dimension of each point in new data:', len(new_data[0]))
new_data = new_data.tolist()
data_file = open('data_avg.csv', 'wb')
for pt in new_data:
    pickle.dump(pt, data_file)
data_file.close()
