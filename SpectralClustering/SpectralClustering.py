# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt

data_file = open('data0.1.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans0.1.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = np.array([row[:-1] for row in data_with_ans])
N = len(data)
print('Number of observations =', N)

print('Calculating pairwise similarity...')
sim_matrix = sklearn.metrics.pairwise.rbf_kernel(data, gamma = 1.0/5.8)
# sim_matrix = sklearn.metrics.pairwise.cosine_similarity(data)
print('...done.')
print('First row of similarity matrix:')
print(sim_matrix[0])
degree_vector = 1/np.sum(sim_matrix, axis = 1, keepdims = True)
degree_matrix = np.eye(N) * degree_vector	# should broadcast
laplacian_rw = np.eye(N) - np.matmul(degree_matrix, sim_matrix)
eigenvals = np.linalg.eigvalsh(laplacian_rw)
# eigenvals = np.linalg.eigvalsh(sim_matrix)
eigenvals = eigenvals.tolist()
eigenvals.sort()
max_gap = -float('inf')
best_k = len(eigenvals)
eigenval_threshold = 0.00001   # should be slightly bigger than 0
# the idea is that eigenvalues of the laplacian_rw will have as many 0s as there are components (clusters).
# for i in range(len(eigenvals) - 1):
#     gap = eigenvals[i + 1] - eigenvals[i]
#     if gap > max_gap:
#         max_gap = gap
#         best_k = i + 1
for i in range(len(eigenvals)):
    if eigenvals[i] > eigenval_threshold:
        best_k = i
        break
print(eigenvals)
print(best_k)
