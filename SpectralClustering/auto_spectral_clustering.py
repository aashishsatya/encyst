# code courtesy of https://github.com/mingmingyang/auto_spectral_clustering.git

from autosp import predict_k
from sklearn.cluster import SpectralClustering
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt

data_file = open('data0.5.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans0.5.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = np.array([row[:-1] for row in data_with_ans])
N = len(data)
print('Number of observations =', N)

print('Calculating pairwise similarity...')
sim_matrix = sklearn.metrics.pairwise.cosine_similarity(data)
print('...done.')

k = predict_k(sim_matrix)
print(k)
