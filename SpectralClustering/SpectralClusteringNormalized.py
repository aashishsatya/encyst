# load the data
import pickle
import numpy as np
import sklearn.metrics
import matplotlib.pyplot as plt
import scipy.spatial.distance

data_file = open('../Data/data0.1.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('../Data/ans0.1.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = np.array([row[:-1] for row in data_with_ans], dtype = np.dtype('float64'))
N = len(data)
print('Number of observations =', N)

# normalize the data
# get the normalization parameters
data_mean = np.mean(data, axis = 0)
print('Calculating pairwise distances...')
data_dist = scipy.spatial.distance.pdist(data, metric = 'euclidean')    # calculate the distance between points
print('...done.')
dist_variance = np.var(data_dist)
print('Variance of the distances =', dist_variance)
# we have the parameters, now actually normalize the data
data -= data_mean
data /= np.sqrt(5 * dist_variance)
# data /= np.sqrt(dist_variance)
# data /= np.sqrt(1024)


print('Calculating pairwise similarity...')
sim_matrix = sklearn.metrics.pairwise.rbf_kernel(data, gamma = 1.0/2)
print('...done.')

degree_vector = 1/np.sum(sim_matrix, axis = 1, keepdims = True)
degree_matrix = np.eye(N) * degree_vector	# should broadcast
laplacian_rw = np.eye(N) - np.matmul(degree_matrix, sim_matrix)
eigenvals = np.linalg.eigvalsh(laplacian_rw)
eigenvals = eigenvals.tolist()
eigenvals.sort()

plt.plot(list(range(1, len(eigenvals) + 1)), eigenvals)
plt.xlabel('k')
plt.ylabel('Eigenvalue')
plt.title('Eigenvalue plot for lambda high = ' + str(0.1))
#plt.show()
plt.savefig('0-1full.svg', format='svg', dpi=1200)

max_gap = -float('inf')
eigenval_thresholds = [0.01, 0.05, 0.001, 0.005, 0.00001, 0.00005]   # should be slightly bigger than 0
eigenval_thresholds.sort()

for eigenval_threshold in eigenval_thresholds:
    best_k = len(eigenvals)
    for i in range(len(eigenvals)):
        if eigenvals[i] > eigenval_threshold:
            best_k = i
            break
    print('k for eigenvalue threshold of', eigenval_threshold, ':', best_k)
