# load the data
import pickle
from collections import Counter
from scipy.spatial import ConvexHull
import time

data_file = open('data.csv', 'rb')
data_with_ans = pickle.load(data_file)
data_file.close()

ans_file = open('ans.csv', 'rb')
ans = pickle.load(ans_file)
ans_file.close()

data = [row[:-1] for row in data_with_ans[:1002]]
print('Computing convex hull...')
start = time.time()
hull = ConvexHull(data)
stop = time.time()
print('...done.')
print('Time taken to compute convex hull =', stop - start)