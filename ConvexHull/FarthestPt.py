# load the data
import pickle
from collections import Counter
import numpy as np

data_file = open('data_avg.csv', 'rb')
text_data = pickle.load(data_file)
data_file.close()

# ans_file = open('ans.csv', 'rb')
# ans = pickle.load(ans_file)
# ans_file.close()

raw_data = np.array(text_data)
N = len(raw_data)
print('Number of data points =', N)
print(raw_data[0])
print('Dimension of each data pt =', len(raw_data[0]))
data = raw_data

# for i in range(N - 1):
#     if i % 100 == 0:
#         print('Averaging with other points for i =', i, '...')
#     for j in range(i + 1, N):
#         data.append((raw_data[i] + raw_data[j]) / 2)

# data_mean = np.mean(data, axis = 0)
# data = data - data_mean # should broadcast
# data_dot = np.inner(data, data)
# maxs = np.max(data_dot, axis = 1)
# indices = set([])
# for i in range(len(data)):
#     i_max_indices = set(np.where(data_dot[i] == maxs[i])[0])
#     # print('i =', i, 'i_max_indices =', i_max_indices)
#     indices = indices.union(i_max_indices)
# print('Number of points found:', len(indices))
# ans_cells = [text_data[i][-1] for i in list(indices)]
# print(Counter(ans_cells))
