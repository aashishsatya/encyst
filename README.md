# Encyst

Encyst is a program that tries to idEntify the Number of Cell tYpes uSing clusTering. More details to follow.

## Acknowledgements

Scripts for concave hull has been cloned from [here](https://github.com/sebastianbeyer/concavehull).

## Updates:

\[Thu Mar 21 15:49:30 IST 2019\] Tried using t-SNE followed by projection onto a circle and then computing the number of extreme points. As expected, got all points back.  
\[Wed Feb 27 11:31:02 IST 2019\] Forgot these updates were a thing. Tried spectral clustering, seems to work conditioned on the assumption that you get the correct similarity function.  
\[Wed Jan 30 11:41:04 IST 2019\] Tried computing the farthest point along each direction after shifting the origin to the mean of all points to see if hulls can be found out that way. Ended up getting all points back.
